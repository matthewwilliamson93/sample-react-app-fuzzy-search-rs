import React from "react";

import FuzzyInput from "./components/FuzzyInput";

/*
 * A React component that contains the framework for the site.
 */
export default function App(): JSX.Element {
    return (
        <div>
            <FuzzyInput />
        </div>
    );
}
