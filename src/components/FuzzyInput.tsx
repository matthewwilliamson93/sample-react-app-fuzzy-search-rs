import Autocomplete from "@mui/material/Autocomplete";
import CircularProgress from "@mui/material/CircularProgress";
import TextField from "@mui/material/TextField";

import { makeStyles } from "@mui/styles";

import React, { useState } from "react";

import { useAsync } from "../hooks/useAsync";
import { useDebounce } from "../hooks/useDebounce";

const useStyles = makeStyles(() => ({
    main: {
        "padding-top": "60px",
    },
    container: {
        display: "flex",
        flexWrap: "wrap",
    },
    cssLabel: {
        color: "black",
        "font-size": 16,
        "font-weight": "bold",
    },
    cssFocused: {
        color: "black",
    },
    notchedOutline: {
        borderWidth: "3px",
        borderColor: "black !important",
    },
}));

/*
 * A React component that is meant to handle the input for search of sites.
 *
 * https://mui.com/components/autocomplete/
 * https://mui.com/components/text-fields/
 * https://mui.com/components/progress/
 */
export default function FuzzyInput(): JSX.Element {
    const { main, container, cssLabel, cssFocused, notchedOutline } =
        useStyles();

    const { loading, value } = useAsync<string[]>(async () => {
        const response = await fetch(
            "https://random-word-api.herokuapp.com/word?number=1000",
            {
                cache: "force-cache",
                method: "GET",
                mode: "cors",
            }
        );
        const results = response.json() as unknown as string[];

        console.log(results);
        return results;
    }, []);

    const [open, setOpen] = useState(false);
    const [query, setQuery] = useState("");
    const debouncedQuery = useDebounce<string>(query, 300);

    const [options, setOptions] = useState<string[]>([]);

    return (
        <div className={main}>
            <Autocomplete
                className={container}
                clearOnEscape
                fullWidth
                open={open}
                onChange={async (event) => {
                    console.log("I guess I could do something with the change");
                    console.log(event);
                }}
                onInputChange={(event) => {
                    if (value === undefined) {
                        return;
                    }

                    /*
                    if (
                        input !== null ||
                        input.length === 0 ||
                        input[input.length - 1] === " "
                    ) {
                        setOptions(value.slice(0, 10));
                        return;
                    }
                    */

                    console.log(
                        "I should filter better with this shouldn't I?"
                    );
                    console.log(event);
                }}
                onOpen={() => {
                    setOpen(true);
                }}
                onClose={() => {
                    setOpen(false);
                }}
                openOnFocus={true}
                getOptionLabel={(option) => option}
                options={options}
                loading={loading}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        autoFocus
                        label="Search"
                        variant="outlined"
                        InputLabelProps={{
                            classes: {
                                root: cssLabel,
                                focused: cssFocused,
                            },
                        }}
                        InputProps={{
                            ...params.InputProps,
                            classes: {
                                focused: cssFocused,
                                notchedOutline: notchedOutline,
                            },
                            endAdornment: (
                                <React.Fragment>
                                    {loading ? (
                                        <CircularProgress
                                            color="inherit"
                                            size={20}
                                        />
                                    ) : null}
                                    {params.InputProps.endAdornment}
                                </React.Fragment>
                            ),
                        }}
                    />
                )}
            />
        </div>
    );
}
