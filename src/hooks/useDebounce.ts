import { useEffect, useState } from "react";

/*
 * A react hook for waiting on a delay to recalulate a value.
 *
 * Arg:
 *      value: The value to recalculate from.
 *      delay: The delay to put on recalculating.
 *
 * Returns:
 *      The value back after a debounced time.
 */
export function useDebounce<T>(value: T, delay?: number): T {
    const [debouncedValue, setDebouncedValue] = useState<T>(value);

    useEffect(() => {
        const timer = setTimeout(() => setDebouncedValue(value), delay || 500);

        return () => {
            clearTimeout(timer);
        };
    }, [value, delay]);

    return debouncedValue;
}
